//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');

var urlClientesMLab = "https://api.mlab.com/api/1/databases/trodriguez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/trodriguez/collections/";

var urlUsuarios = "https://api.mlab.com/api/1/databases/trodriguez/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var urlMovs = "https://api.mlab.com/api/1/databases/trodriguez/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

//var usuarioMLab = requestjson.createUsuario(urlUsuarios);

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLabRaiz;

var clienteMLab = requestjson.createClient(urlClientesMLab);

var movimientosJSON = require('./movimientosv2.json');

var bodyparser = require('body-parser');

//var movMlab = requestjson.createMovtos(urlMovs);

app.use(bodyparser.json());

app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Header", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function (req, res) {
res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/', function (req, res) {
  res.send('Hemos recibido su petición POST');
})

app.put('/', function (req, res) {
  res.send('Hemos recibido su petición PUT');
})

app.delete('/', function (req, res) {
  res.send('Hemos recibido su petición Delete');
})

app.get('/Clientes/:idcliente',function (req, res) {
res.send('Aquí tiene al cliente número' + req.params.idcliente);
})

//app.get('/Movimientos',function (req, res) {
//res.sendfile('movimientosv1.json');
//})

app.get('/v2/Movimientos',function (req, res) {
res.json(movimientosJSON);
})

app.get('/v2/Movimientos/:id',function (req, res) {
  console.log(req.params.id);
res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/Movimientosq',function (req, res) {
  console.log(req.query);
res.send('Query Recibido'+ req.query);
})

app.post('/v2/Movimientos', function (req, res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length+1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})

app.get ('/v1/Clientes', function (req, res) {
var clienteMLab = requestjson.createClient(urlClientesMLab);
  clienteMLab.get('',function(err, resM, body){
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
    })
})

app.post('/v1/Clientes', function(req, res)
{
  clienteMLab.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})

app.post('/v2/Login', function(req, res){
  console.log(req);
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q= {"email": "'+ email +'","password":"'+ password +'"}';
  console.log(query);

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query)

  console.log(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);

  clienteMLabRaiz.get('', function(err, resM, body){
    if (!err) {
      if (body.length ==1) {  //Login OK
        res.status(200).send('Usuario logado');
      }else {
        res.status(404).send('Usuario Inexistente');
      }
    }
  })
})

//Se debe llamar a post para la Creación de la cuenta
app.post('/v1/Create', function(req, res){
  //console.log(req);
  var nombre = req.body.nombre;
  var apellido = req.body.apellido;
  var email = req.body.email;
  var password = req.body.password;
  var query = '{"nombre": "'+ nombre +'","apellido": "'+ apellido +'","password":"'+ password +'"}';
  console.log(query);

  var data  = urlMLabRaiz + "Usuarios?" + apiKey;

  console.log(data);

  clienteMLabRaiz = requestjson.createClient(data)
  clienteMLabRaiz.post('', req.body, function(err, resM, body){
      if(err){
      console.log(err);}
      //console.log(err);}
    else{
      res.send(query);
    }

  });
})

//Se debe llamar a post para el registro de la operación
app.post('/v3/Movimientos', function(req, res){
  var email = req.body.email;
  var noCuenta = req.body.noCuenta;
  var fechaOper = req.body.fechaOper;
  var tipoOper = req.body.tipoOper;
  var desCorta = req.body.desCorta;
  var monto = req.body.monto;
  var emisora = req.body.emisora;
  var serie = req.body.serie;
  var titulos = req.body.titulos;
  var query = '{"email": "'+ email +'","noCuenta": "'+ noCuenta +'","fechaOper":"'+ fechaOper +'","tipoOper":"'+ tipoOper +'","desCorta":"'+ desCorta +'","monto":"'+ monto +'","emisora":"'+ emisora +'","serie":"'+ serie +'","titulos":"'+ titulos +'"}';
  console.log(query);
  var data  = urlMLabRaiz + "Movimientos?" + apiKey;

  console.log(data);
  clienteMLabRaiz = requestjson.createClient(data) //
  clienteMLabRaiz.post('', req.body, function(err, resM, body){

    if(err){console.log(err);}
    else{
      res.send(query);
    }

    });

})

app.post('/v1/Recuperacion', function(req, res){
  console.log(req);
  var email = req.body.email;
  var respSec = req.body.respSec;
  var query = 'q= {"email": "'+ email +'","respSec":"'+ respSec +'"}';
  console.log(query);

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Recuperacion?" + apiKey + "&" + query)

  console.log(urlMLabRaiz + "Recuperacion?" + apiKey + "&" + query);

  clienteMLabRaiz.get('', function(err, resM, body){
    if (!err) {
      if (body.length ==1) {  //Contraseña recuperada
        res.status(200).send('Recuperada');
      }else {
        res.status(404).send('Datos Incorrectos1');
      }
    }
  })
})

//Obtiene nombre del Usuario
app.get ("/Usuarios/:email",function(req, res) {
var url = urlUsuarios;
if(req.params.email!=undefined && req.params.email!=''){
var name = req.params.email;
name = name.replace("email", "\"email\"");
name = name.replace("=", ":");
url = url + "&q={"+name+"}";

}
var cliprodMLab = requestjson.createUsuario(url);
cliprodMLab.get('',function(err,resM,body){
if(err){console.log(body);}
else{
res.send(body);
}
});
})

//Obtiene Movimientos con base en el email

app.get ("/Movimientos/:email",function(req, res) {
var url = "https://api.mlab.com/api/1/databases/trodriguez/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var query = '&q= {"email": "'+ req.params.email +'"}';
var cliprodMLab = requestjson.createClient(url + query);

cliprodMLab.get('',function(err,resM,body){
if(err){console.log(body);}
else{
res.send(body);
}
});
})

//muestra la lista de sucursales
app.get ("/Sucursales",function(req, res) {
var url = "https://apis.bbvabancomer.com/locations_sbx/v1/atms";
var query1 = '?= {"longitude": "'+ req.headers['longitude'] +'"}';
//var query1 = req.headers['longitude'];
var query2 = '?= {"latitude": "'+ req.headers['latitude'] +'"}';
//console.log(query1);
var sucMLab = requestjson.createClient(url + query1 + query2);
sucMLab.headers['Content-Type'] = 'application/json';
sucMLab.headers['Authorization'] = 'jwt eyJ6aXAiOiJERUYiLCJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiUlNBLU9BRVAifQ.LGzsxc9O5b07e1vrhK7-2HMhoUNKIPshx1_mUJY40wGgmrEwQ8fyKr6U9nrB7zVUER8g0qro9Vjgj6VzhSqemOlivDJicuV5qtw4TQEH-uUoP5K-ManQeyfp4g0cGErpiuPY7mdrRHC6PLo1hKxON3_6gWAUTI6ku5ftKN3yJiYTiRZpW8FjJ6sqXwggnkd9wJxEkoNEoIs-QmBTcNfhmBnvAHBR5oGFGKRUyB7WNZVmqnheeChuldwKDT7CK8E4LA3E6eN67Ti9AlOeykqiSOmDjfweg8uvEqBUkHhi00-DFIwahPdereXBMaFebbtY4S1z4-03mz2W2HjSgtKQgw.rSsQJ1bIiYjDzZ26.PUEuzNzZBuGm1J1FgkPBX04TvjPhuw2rInRhlFVcFLcf-qA3RhfE0AeYm28JeFwysz6r9Dixyj07v1Bc2AV4viCMFTqtDwcOEfQWV7QYUig3UljJI9sXtEBHkA6Qlc7LP9ThhMK5A0NLDP-TodD8s1vYp-oN55aYpFWbRo2M6Lh3bg2Yt4zfzgk03Y-0jPD6_XJeKksdoYC9nY7AxodZzn2dxymoVDY2.EMRV3hSmTl2AphKPxvL8HA';
//sucMLab.headers['longitude'] = req.headers['longitude'];
//sucMLab.headers['latitude'] = req.headers['latitude'];

sucMLab.get('',function(err,resM,body){
 if(err){console.log(body);}
else{
res.send(body);
}
});
})
